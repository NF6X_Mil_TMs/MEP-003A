# Misc Documentation

This directory contains misc. documentation for the MEP-003A generator.

Description                                      | File
-------------------------------------------------|-----------------------------------------------------------
Fuel injection pump service instructions         | [DieselFuelInjectionPump.pdf](DieselFuelInjectionPump.pdf)
Fuel injection pump parts diagram                | [9540A.pdf](9540A.pdf)
Timing button selection and installation         | [IF3136.pdf](IF3136.pdf)
PS Magazine, 1990-09: Preventing starter burnout | [StarterBurnoutDED.pdf](StarterBurnoutDED.pdf)
