# MEP-003A 10kW Diesel Engine Driven Generator Set

This repository contains my curated cache of technical manuals for the MEP-003A 10kW diesel engine driven generator set.

![MEP-003A Line Drawing](MEP-003A.png)

The documents are sorted into these subdirectories:

Directory              | Description
-----------------------|---------------------------------------
[TM](TM/README.md)     | Official US Military Technical Manuals
[misc](misc/README.md) | Miscellaneous Documents
