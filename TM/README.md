# MEP-003A Technical Manuals

This directory contains official technical manuals for the MEP-003A generator.

## Technical Manuals: MEP-003A Generator Set and Components

Title                                      | Manual                                      | Date
-------------------------------------------|---------------------------------------------|-----------
Operation and Organizational Maintenance   | [TM 5-6115-585-12](TM5-6115-585-12.pdf)     | 1977-07-25
Field and Depot Maintenance                | [TM 5-6115-585-34](TM5-6115-585-34.pdf)     | 1977-07-25
Parts                                      | [TM 5-6115-585-24P](TM5-6115-585-24P.pdf)   | 1983-10-20
Lubrication Order                          | [LO 5-6115-585-12](LO5-6115-585-12.pdf)     | 1990-06-15
Maintenance & Parts, DJEAM & DJBMA Engines | [TM 9-2815-221-34&P](TM9-2815-221-34+P.pdf) | 1985-10-11


## Technical Manuals: Systems Including MEP-003A Generator Set

Title                                     | Manual                                      | Date
------------------------------------------|---------------------------------------------|-----------
Operation, Maintenance & Parts, PU-753/M  | [TM 5-6115-632-14&P](TM5-6115-632-14+P.pdf) | 1988-06-17
Operation, Maintenance & Parts, AN/MJQ-18 | [TM 5-6115-633-14&P](TM5-6115-633-14+P.pdf) | 1988-06-13
